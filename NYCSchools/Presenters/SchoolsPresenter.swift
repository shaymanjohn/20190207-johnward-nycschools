//
//  SchoolsPresenter.swift
//  NYCSchools
//
//  Created by John Ward on 06/02/2019.
//  Copyright © 2019 John Ward. All rights reserved.
//

import Foundation
import UIKit

enum APIEndPoint : String {
    case schoolsList = "/resource/97mf-9njv.json"
    case gradesList = "resource/734v-jeq5.json"
}

class SchoolsPresenter: SchoolsProtocol, APIRequester {
    
    var baseUrl = URL(string: "https://data.cityofnewyork.us")
    let storage: CoreData
    
    var schools: [School]? {
        return storage.schools
    }
    
    init(storeInMemory: Bool = false) {
        
        storage = CoreData(storeInMemory: storeInMemory)
    }
    
    var filteredSchools: [School] = []
    
    func updateSchools(session: URLSession, completion: @escaping ((Result<Void>) -> ())) {
        
        guard let request = getRequest(to: .schoolsList) else {
            completion(.failure(APIError.schoolAPIError))
            return
        }
        
        session.dataTask(with: request) { [weak self] data, response, error in
            if let data = data,
                let schools = try? JSONDecoder().decode([School].self, from: data) {
                
                self?.storage.updateSchools(schools)
                
                // Return to UI on main.
                DispatchQueue.main.async {
                    completion(.success(()))
                }
            } else {
                DispatchQueue.main.async {
                    if let error = error {
                        completion(.failure(error))
                    } else {
                        completion(.failure(APIError.schoolAPIError))
                    }
                }
            }
            }.resume()
    }
    
    func updateSchoolGrades(school: School, session: URLSession, completion: @escaping ((Result<School>) -> ())) {
        
        guard let request = getRequest(to: .gradesList, parameters: ["dbn" : school.dbn]) else {
            completion(.failure(APIError.schoolAPIError))
            return
        }
        
        session.dataTask(with: request) { [weak self] data, response, error in
            if let data = data,
                let schoolGrades = try? JSONDecoder().decode([SchoolGrades].self, from: data),
                let grades = schoolGrades.first {
                
                self?.storage.updateSchool(school, with: grades)
                let updatedSchool = School(name: school.school_name, dbn: school.dbn, grades: grades, city: school.city, latitude: school.latitude, longitude: school.longitude, neighborhood: school.neighborhood, website: school.website, zip: school.zip)
                
                // Return to UI on main.
                DispatchQueue.main.async {
                    completion(.success(updatedSchool))
                }
            } else {
                DispatchQueue.main.async {
                    if let error = error {
                        completion(.failure(error))
                    } else {
                        completion(.failure(APIError.gradesAPIError))
                    }
                }
            }
            }.resume()
    }
}
