//
//  MockSchoolsPresenter.swift
//  NYCSchools
//
//  Created by John Ward on 06/02/2019.
//  Copyright © 2019 John Ward. All rights reserved.
//

import Foundation

class MockSchoolsPresenter: SchoolsProtocol {

    var schools: [School]? = []    
    var filteredSchools: [School] = []
    
    func updateSchools(session: URLSession, completion: @escaping ((Result<Void>) -> ())) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {

            let grades1 = SchoolGrades("78", reading: "74.1", maths: "68.8", writing: "79.0")
            let grades2 = SchoolGrades("56", reading: "94.1", maths: "88.2", writing: "81.9")

            let school1 = School(name: "Brooksbank School", dbn: "1", grades: grades1, city: "Halifax", latitude: "53.7201", longitude: "-1.8107", neighborhood: "Elland", website: "https://www.bbs.calderdale.sch.uk/", zip: "HX4 8HU")
            let school2 = School(name: "Greetland School", dbn: "2", grades: grades2, city: "Halifax", latitude: "53.686538", longitude: "-1.872", neighborhood: "Greetland", website: "https://www.greetlandacademy.org.uk/calderdale/primary/rghmat", zip: "HX4 8HU")

            self.schools = [school1, school2]

            completion(.success(()))
        })
    }

    func updateSchoolGrades(school: School, session: URLSession, completion: @escaping ((Result<School>) -> ())) {

        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
            completion(.success(school))
        })
    }

}
