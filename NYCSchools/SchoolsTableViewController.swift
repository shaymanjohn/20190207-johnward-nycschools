//
//  SchoolsTableViewController.swift
//  NYCSchools
//
//  Created by John Ward on 06/02/2019.
//  Copyright © 2019 John Ward. All rights reserved.
//

import UIKit
 
class SchoolsTableViewController: ActivityTableViewController, UISearchBarDelegate, UISearchResultsUpdating {
    
    // This presenter could be injected
    let presenter: SchoolsProtocol = SchoolsPresenter() // MocksSchoolsPresenter()
    let searchController: UISearchController = UISearchController(searchResultsController: nil)
    
    var tableIsFiltered: Bool {
        return searchController.isActive
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Add the search bar (UISearchController can't be added in Interface Builder).
        // Could use a seperate controller for this, but its closely coupled.
        searchController.searchResultsUpdater = self
        searchController.dimsBackgroundDuringPresentation = false
        searchController.searchBar.delegate = self
        searchController.searchBar.placeholder = NSLocalizedString("SearchSchools", comment: "Search for a school")
        searchController.searchBar.tintColor = .white
        navigationItem.searchController = searchController
        navigationItem.hidesSearchBarWhenScrolling = false      // Always show search

        // Configure table and spinner first time in.
        configureTable()
        refreshSchools()
    }

    /// Called when loading view and when schools are updated.
    func configureTable() {

        if !presenter.hasSchools {
            spinner.startAnimating()
        }

        // Edge case - user is waiting for data and has typed search criteria.
        if tableIsFiltered {
            presenter.filterSchools(on: searchController.searchBar.text)
        }

        tableView.reloadData()
    }
    
    /// Pull schools list and update local storage
    @IBAction func refreshSchools(_ sender: UIRefreshControl? = nil) {
        presenter.updateSchools(session: URLSession.shared) { [weak self] result in
            
            switch result {
            case .success:
                self?.configureTable()
                
            case .failure(let error):
                self?.showError(error)
            }
            
            self?.tableView.refreshControl?.endRefreshing()
            self?.spinner.stopAnimating()
        }
    }
    
    // MARK: - Search controller delegate methods
    func updateSearchResults(for searchController: UISearchController) {
        presenter.filterSchools(on: searchController.searchBar.text)
        tableView.reloadData()
    }
    
    // MARK: - Table view data source
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableIsFiltered {
            return presenter.filteredSchools.count
        }

        return presenter.schools?.count ?? 0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {        
        if let cell = tableView.dequeueReusableCell(withIdentifier: "schoolCell") as? SchoolTableViewCell,
            let school = tableIsFiltered ? presenter.filteredSchools[indexPath.row] : presenter.schoolAtIndex(indexPath.row) {
            cell.configure(school)
            return cell
        }

        // If we've got here, there's something badly wrong with the build. No problem with fatalError in this circumstance (it's a build issue).
        fatalError("Build error: Missing cell on storyboard")
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let school = tableIsFiltered ? presenter.filteredSchools[indexPath.row] : presenter.schoolAtIndex(indexPath.row),
            let detailView = self.storyboard?.instantiateViewController(withIdentifier: "gradesView") as? GradesViewController {
            detailView.school = school
            detailView.presenter = presenter
            navigationController?.pushViewController(detailView, animated: true)
        }
    }
}
