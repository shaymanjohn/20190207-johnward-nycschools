//
//  SchoolTableViewCell.swift
//  NYCSchools
//
//  Created by John Ward on 06/02/2019.
//  Copyright © 2019 John Ward. All rights reserved.
//

import UIKit

class SchoolTableViewCell: UITableViewCell {
    
    @IBOutlet weak var nameLabel: UILabel?
    @IBOutlet weak var locationLabel: UILabel?

    func configure(_ school: School) {
        
        nameLabel?.text = school.school_name
        locationLabel?.text = school.locationDescription
    }

}
