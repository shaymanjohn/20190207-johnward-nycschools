//
//  MapTableViewCell.swift
//  NYCSchools
//
//  Created by John Ward on 07/02/2019.
//  Copyright © 2019 John Ward. All rights reserved.
//

import UIKit
import MapKit

class MapTableViewCell: UITableViewCell {
    
    @IBOutlet weak var mapView: MKMapView?
    
    func configure(_ school: School) {
        
        // Center on school location (if present)
        if let long = school.longitude,
            let lat = school.latitude,
            let longitude = Double(long),
            let latitude = Double(lat) {
            
            let location = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
            let span = MKCoordinateSpan(latitudeDelta: 0.02, longitudeDelta: 0.02)
            let region = MKCoordinateRegion(center: location, span: span)
            mapView?.setRegion(region, animated: false)
            
            
            // And drop a pin on it.
            let annotation = MKPointAnnotation()
            annotation.coordinate = location
            annotation.title = school.school_name
            mapView?.addAnnotation(annotation)
        }
    }
}
