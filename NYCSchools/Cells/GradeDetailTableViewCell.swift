//
//  GradeDetailTableViewCell.swift
//  NYCSchools
//
//  Created by John Ward on 07/02/2019.
//  Copyright © 2019 John Ward. All rights reserved.
//

import UIKit

class GradeDetailTableViewCell: UITableViewCell {

    @IBOutlet weak var statLabel: UILabel?
    @IBOutlet weak var valueLabel: UILabel?

    func configure(_ grades: SchoolGrades?, gradeType: SchoolGrades.gradeType) {

        let statText: String
        let valueText: String?

        switch gradeType {

        case .numTakers:
            statText = NSLocalizedString("GradesNumTakers", comment: "Number of takers")
            valueText = grades?.num_of_sat_test_takers

        case .mathsAverage:
            statText = NSLocalizedString("GradesMathsScore", comment: "Maths score")
            valueText = grades?.sat_math_avg_score

        case .readingAverage:
            statText = NSLocalizedString("GradesReadingScore", comment: "Reading score")
            valueText = grades?.sat_critical_reading_avg_score

        case .writingAverage:
            statText = NSLocalizedString("GradesWritingScore", comment: "Writing score")
            valueText = grades?.sat_writing_avg_score
        }

        statLabel?.text = statText
        valueLabel?.text = valueText ?? ""
    }
    
}
