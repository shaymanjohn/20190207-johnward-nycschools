//
//  GradeHeaderTableViewCell.swift
//  NYCSchools
//
//  Created by John Ward on 07/02/2019.
//  Copyright © 2019 John Ward. All rights reserved.
//

import UIKit

class GradeHeaderTableViewCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel?

    func configure(_ school: School) {

        nameLabel?.text = school.school_name
    }
    
}
