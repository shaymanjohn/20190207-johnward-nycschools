//
//  Result.swift
//  NYCSchools
//
//  Created by John Ward on 06/02/2019.
//  Copyright © 2019 John Ward. All rights reserved.
//

import Foundation

enum Result<T> {
    
    case success(T)
    case failure(Error)
    
    var error: Error? {
        switch self {
        case .success:
            return nil
        case .failure(let e):
            return e
        }
    }
    
    var value: T? {
        switch self {
        case .success(let v):
            return v
        case .failure:
            return nil
        }
    }
}

