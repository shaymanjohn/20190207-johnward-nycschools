//
//  APIRequester.swift
//  NYCSchools
//
//  Created by John Ward on 06/02/2019.
//  Copyright © 2019 John Ward. All rights reserved.
//

import Foundation

protocol APIRequester {
    
    /// Base API URL
    var baseUrl: URL? { get set }
    
    /// Get request object for given path on base URL.
    func getRequest(to path: APIEndPoint, parameters: [String : String]) -> URLRequest?
}

extension APIRequester {
    func getRequest(to path: APIEndPoint, parameters: [String : String] = [:]) -> URLRequest? {
        
        if let url = URL(string: path.rawValue, relativeTo: baseUrl) {

            var comps = URLComponents(url: url, resolvingAgainstBaseURL: true)!
            comps.queryItems = parameters.map { URLQueryItem(name: $0.key, value: $0.value)}

            var request = URLRequest(url: comps.url!)
            request.httpMethod = "GET"
            request.cachePolicy = .reloadIgnoringLocalCacheData
            request.timeoutInterval = 10.0

            return request
        }
        
        return nil
    }
}

enum APIError: Error {
    static let schoolAPIError = NSError(domain: "SchoolsAPI", code: 1, userInfo: [NSLocalizedDescriptionKey : NSLocalizedString("APISchoolsError", comment: "API Error")])
    static let gradesAPIError = NSError(domain: "SchoolsAPI", code: 2, userInfo: [NSLocalizedDescriptionKey : NSLocalizedString("APIGradesError", comment: "API Error")])
}
