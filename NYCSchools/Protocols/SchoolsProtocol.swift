//
//  SchoolsProtocol.swift
//  NYCSchools
//
//  Created by John Ward on 06/02/2019.
//  Copyright © 2019 John Ward. All rights reserved.
//

import Foundation

protocol SchoolsProtocol: class {
    
    /// List of schools in local store.
    var schools: [School]? { get }
    var filteredSchools: [School] { get set }
    var hasSchools: Bool { get }
    
    /// Filter schools list based on user input
    func filterSchools(on: String?)
    
    /// Pull schools list from API and update local store.
    func updateSchools(session: URLSession, completion: @escaping ((Result<Void>) -> ()))

    /// Pull a schools grade info from API and update local store.
    func updateSchoolGrades(school: School, session: URLSession, completion: @escaping ((Result<School>) -> ()))
    
    /// School for given array index (or nil if not found).
    func schoolAtIndex(_ index: Int) -> School?
}

extension SchoolsProtocol {
    
    var hasSchools: Bool {
        if let schools = schools,
            schools.count > 0 {
            return true
        }
        
        return false
    }
    
    func filterSchools(on filter: String?) {
        let textCount = filter?.count ?? 0
        
        // Empty search string, just return the full list (so that user isn't presented with empty list at start of search).
        if (textCount == 0) || (schools == nil) {
            filteredSchools = schools ?? []
        } else {
            // Search in school name and location.
            filteredSchools = schools?.filter({ $0.school_name.lowercased().contains(filter!.lowercased()) ||
                $0.locationDescription!.lowercased().contains(filter!.lowercased())
            }) ?? []
        }
    }

    func schoolAtIndex(_ index: Int) -> School? {
        if let schools = schools,
            index < schools.count,
            index >= 0 {
            
            return schools[index]
        }
        
        return nil
    }    
}

