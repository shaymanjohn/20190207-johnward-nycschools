//
//  GradesViewController.swift
//  NYCSchools
//
//  Created by John Ward on 07/02/2019.
//  Copyright © 2019 John Ward. All rights reserved.
//

import UIKit

class GradesViewController: ActivityTableViewController {

    var school: School!
    var presenter: SchoolsProtocol!

    override func viewDidLoad() {

        super.viewDidLoad()

        // Check mandatory parameters have been passed in...
        guard school != nil, presenter != nil else {
            fatalError("Parameter missing in grade view")
        }

        // If not already pulled, show spinner (either way we're going to pull in case of updates to grades).
        if school.grades == nil {
            spinner.startAnimating()
        }

        refreshGrades()
    }

    /// Pull grades for this school
    @IBAction func refreshGrades(_ sender: UIRefreshControl? = nil) {

        presenter.updateSchoolGrades(school: school, session: URLSession.shared) { [weak self] result in

            switch result {
            case .success(let school):
                self?.school = school

            case .failure(let error):
                self?.showError(error)
            }

            self?.tableView.reloadData()
            self?.tableView.refreshControl?.endRefreshing()
            self?.spinner.stopAnimating()
        }
    }

    /// Open webpage for the school
    @IBAction func openWebpage() {
        openUrl(school.website)
    }

    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {

        if let cell = tableView.dequeueReusableCell(withIdentifier: "gradeHeader") as? GradeHeaderTableViewCell {
            cell.configure(school)
            return cell.contentView
        }

        return nil
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        // A cell for all grade types, plus an extra one for the map.
        return SchoolGrades.gradeType.allCases.count + 1
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        if let gradeType = SchoolGrades.gradeType.init(rawValue: indexPath.row),
            let cell = tableView.dequeueReusableCell(withIdentifier: "gradeDetail") as? GradeDetailTableViewCell {

            cell.configure(school.grades, gradeType: gradeType)
            return cell
        }

        // Last row is the map.
        if indexPath.row == SchoolGrades.gradeType.allCases.count {
            if let cell = tableView.dequeueReusableCell(withIdentifier: "mapCell") as? MapTableViewCell {
                
                cell.configure(school)
                return cell
            }
        }

        // If we've got here, there's something badly wrong with the build. No problem with fatalError in this circumstance (it's a build issue).
        fatalError("Build error: Missing cell on storyboard")
    }
}
