//
//  SchoolGrades.swift
//  NYCSchools
//
//  Created by John Ward on 07/02/2019.
//  Copyright © 2019 John Ward. All rights reserved.
//

import Foundation

struct SchoolGrades: Codable {

    let num_of_sat_test_takers: String?
    let sat_critical_reading_avg_score: String?
    let sat_math_avg_score: String?
    let sat_writing_avg_score: String?

    init(_ numTakers: String, reading: String, maths: String, writing: String) {
        self.num_of_sat_test_takers = numTakers
        self.sat_critical_reading_avg_score = reading
        self.sat_math_avg_score = maths
        self.sat_writing_avg_score = writing
    }

    // CaseIterable give us a collection of the enums, useful for the detail view.
    enum gradeType: Int, CaseIterable {
        case numTakers = 0
        case mathsAverage = 1
        case readingAverage = 2
        case writingAverage = 3
    }
}
