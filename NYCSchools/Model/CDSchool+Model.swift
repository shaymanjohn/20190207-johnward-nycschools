//
//  CDSchool+Model.swift
//  NYCSchools
//
//  Created by John Ward on 06/02/2019.
//  Copyright © 2019 John Ward. All rights reserved.
//

import Foundation
import CoreData

extension CDSchool {

    /// Update core data object from school model.
    func updateWithModel(_ school: School) {

        self.dbn = school.dbn
        self.name = school.school_name
        self.latitude = school.latitude
        self.longitude = school.longitude
        self.city = school.city
        self.neighborhood = school.neighborhood
        self.website = school.website
        self.zip = school.zip
    }

    func updateWithGradeModel(_ grades: SchoolGrades) {

        guard let context = managedObjectContext,
            let gradesEntity = NSEntityDescription.entity(forEntityName: "CDGrades", in: context) else { return }

        if let cdGrades = NSManagedObject(entity: gradesEntity, insertInto: context) as? CDGrades {
            cdGrades.numberSatTakers = grades.num_of_sat_test_takers
            cdGrades.satMathsScore = grades.sat_math_avg_score
            cdGrades.satReadingScore = grades.sat_critical_reading_avg_score
            cdGrades.satWritingScore = grades.sat_writing_avg_score
            
            self.grades = cdGrades
        }
    }
}

extension School {

    // Convenience init for converting core data object back to a school model (failable).
    init?(_ cdSchool: CDSchool) {

        if let name = cdSchool.name, let dbn = cdSchool.dbn, let city = cdSchool.city,
            let latitude = cdSchool.latitude, let longitude = cdSchool.longitude,
            let neighborhood = cdSchool.neighborhood, let website = cdSchool.website, let zip = cdSchool.zip {

            self.school_name = name
            self.dbn = dbn
            self.city = city
            self.latitude = latitude
            self.longitude = longitude
            self.neighborhood = neighborhood
            self.website = website
            self.zip = zip

            if let grades = cdSchool.grades {
                self.grades = SchoolGrades(grades)
            } else {
                self.grades = nil
            }
        } else {
            return nil
        }
    }
}

extension SchoolGrades {

    init?(_ cdGrade: CDGrades) {

        self.num_of_sat_test_takers = cdGrade.numberSatTakers
        self.sat_math_avg_score = cdGrade.satMathsScore
        self.sat_critical_reading_avg_score = cdGrade.satReadingScore
        self.sat_writing_avg_score = cdGrade.satWritingScore
    }
}
