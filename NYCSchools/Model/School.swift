//
//  School.swift
//  NYCSchools
//
//  Created by John Ward on 06/02/2019.
//  Copyright © 2019 John Ward. All rights reserved.
//

import Foundation

// Could create Codable keys here and use our own naming convention, rather than relying on the API names...
// I've cherry picked the ones I'm interested in here, there are lots more.

struct School: Codable, Equatable {

    let dbn: String
    let school_name: String
    let grades: SchoolGrades?
    let city: String?
    let latitude: String?
    let longitude: String?
    let neighborhood: String?
    let website: String?
    let zip: String?

    init(name: String, dbn: String, grades: SchoolGrades, city: String?, latitude: String?, longitude: String?, neighborhood: String?, website: String?, zip: String?) {

        self.dbn = dbn
        self.school_name = name
        self.grades = grades
        self.city = city
        self.latitude = latitude
        self.longitude = longitude
        self.neighborhood = neighborhood
        self.website = website
        self.zip = zip
    }

    // Convenience variable, used to display in main list and in search.
    var locationDescription: String? {
        return [neighborhood, city, zip].compactMap { $0 }.filter { !$0.isEmpty }.joined(separator: ", ")
    }
    
    public static func ==(school1: School, school2: School) -> Bool {
        return school1.dbn == school2.dbn &&
        school1.school_name == school2.school_name        
    }
}
