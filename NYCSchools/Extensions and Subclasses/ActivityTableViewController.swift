//
//  ActivityTableViewController.swift
//  NYCSchools
//
//  Created by John Ward on 06/02/2019.
//  Copyright © 2019 John Ward. All rights reserved.
//

import UIKit

class ActivityTableViewController: UITableViewController {
    
    /// Returns the activity indicator for the view.
    let spinner = UIActivityIndicatorView(style: .whiteLarge)

    override func viewDidLoad() {        
        super.viewDidLoad()
        
        // Configure an activity spinner, pinned to middle of screen (and hidden when not active).
        spinner.hidesWhenStopped = true
        spinner.color = .gray
        spinner.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(spinner)
        
        NSLayoutConstraint.activate([
            spinner.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            spinner.centerYAnchor.constraint(equalTo: view.centerYAnchor),
            ])
    }
}
