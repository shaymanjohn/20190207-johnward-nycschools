//
//  UIViewController+Alert.h
//  NYCSchools
//
//  Created by John Ward on 07/02/2019.
//  Copyright © 2019 John Ward. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIViewController (Alert)
-(void)showError:(NSError *)error;
@end

NS_ASSUME_NONNULL_END
