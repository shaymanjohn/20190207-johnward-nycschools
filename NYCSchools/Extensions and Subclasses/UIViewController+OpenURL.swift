//
//  UIViewController+OpenURL.swift
//  NYCSchools
//
//  Created by John Ward on 07/02/2019.
//  Copyright © 2019 John Ward. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {

    func openUrl(_ urlString: String?) {
         
        if let urlString = urlString {

            let urlToOpen: String

            if !urlString.hasPrefix("http://") && !urlString.hasPrefix("https://") {
                urlToOpen = "http://" + urlString   // this could be https, but http should redirect to https too.
            } else {
                urlToOpen = urlString
            }

            if let url = URL(string: urlToOpen) {
                if UIApplication.shared.canOpenURL(url) {
                    UIApplication.shared.open(url, options: [:], completionHandler: { _ in })
                }
            }
        }
    }
}
