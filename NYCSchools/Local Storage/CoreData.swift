//
//  CoreData.swift
//  NYCSchools
//
//  Created by John Ward on 06/02/2019.
//  Copyright © 2019 John Ward. All rights reserved.
//

import Foundation
import CoreData

class CoreData {

    var memoryStore: Bool

    /// Initialise with storage type (memory store used for tests)
    init(storeInMemory: Bool = false) {

        memoryStore = storeInMemory
    }

    lazy var persistentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: "schools")
        
        if memoryStore {
            let description = NSPersistentStoreDescription()
            description.type = NSInMemoryStoreType
            container.persistentStoreDescriptions = [description]
        }

        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {

                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })

        container.viewContext.automaticallyMergesChangesFromParent = true
        return container
    }()

    var schools: [School]? {
        let fetchschools = NSFetchRequest<NSFetchRequestResult>(entityName: "CDSchool")
        
        if let allObjects = try? persistentContainer.viewContext.fetch(fetchschools),
            let allSchools = allObjects as? [CDSchool] {
            
            return allSchools.sorted(by: {$0.name!.lowercased() < $1.name!.lowercased()}).compactMap( { School($0) })
        }

        return nil
    }

    func updateSchools(_ schools: [School]) {
        let backgroundContext = persistentContainer.newBackgroundContext()

        // All network ops on background thread and background context.
        backgroundContext.performAndWait {

            if let schoolEntity = NSEntityDescription.entity(forEntityName: "CDSchool", in: backgroundContext) {

                for school in schools {

                    let fetchschool = NSFetchRequest<NSFetchRequestResult>(entityName: "CDSchool")
                    fetchschool.predicate = NSPredicate(format: "dbn = %@", school.dbn)

                    do {
                        if let existingSchool = try backgroundContext.fetch(fetchschool).first as? CDSchool {
                            existingSchool.updateWithModel(school)
                        } else if let newSchool = NSManagedObject(entity: schoolEntity, insertInto: backgroundContext) as? CDSchool {
                            newSchool.updateWithModel(school)
                        }
                    } catch {
                        let nserror = error as NSError
                        fatalError("Core Data error \(nserror), \(nserror.userInfo)")
                    }
                }

                if backgroundContext.hasChanges {
                    try? backgroundContext.save()
                }

            }
        }
    }

    func updateSchool(_ school: School, with grades: SchoolGrades) {

        let backgroundContext = persistentContainer.newBackgroundContext()

        backgroundContext.performAndWait {

            // First get the school whose grades we've got...
            let fetchschool = NSFetchRequest<NSFetchRequestResult>(entityName: "CDSchool")
            fetchschool.predicate = NSPredicate(format: "dbn = %@", school.dbn)

            do {
                if let existingSchool = try backgroundContext.fetch(fetchschool).first as? CDSchool {
                    existingSchool.updateWithGradeModel(grades)
                }
            } catch {
                let nserror = error as NSError
                fatalError("Core Data error \(nserror), \(nserror.userInfo)")
            }

            if backgroundContext.hasChanges {
                try? backgroundContext.save()
            }
        }
    }

}
