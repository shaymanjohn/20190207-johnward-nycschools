# NYC Schools

**Demo project for JPMorgan.**

* MVP.
* Local storage is core data.
* Dynamic fonts.
* Readable width for iPad (landscape).
* Mock presenter if API is down).

**Suggested improvements and future features**

* Certificate pinning could be implemented, but risky when using 3rd party API's like this (3rd party can update their certificate at any time).
* iPad is using same UI as phone, could be richer (e.g. split view controller, etc).
* Storyboards to be localized.
* Could move some of the tableview delegate/datasource methods into their own class(es). (for simple case, and vc around 100 lines I'm ok with it)
* Main schools tableview could be indexed and/or sectioned.
* UI tests 
