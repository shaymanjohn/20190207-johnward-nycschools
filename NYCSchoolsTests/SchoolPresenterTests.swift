//
//  SchoolPresenterTests.swift
//  NYCSchoolsTests
//
//  Created by John Ward on 07/02/2019.
//  Copyright © 2019 John Ward. All rights reserved.
//

import XCTest
@testable import NYCSchools

class SchoolPresenterTests: XCTestCase {
    
    let schoolCreateCount = 3

    override func setUp() {
    }

    override func tearDown() {
    }

    func testCreateSchools() {
        
        // given
        let presenter = SchoolsPresenter(storeInMemory: true)
        
        // when
        addSomeSchools(to: presenter)
        
        // then
        let count = presenter.schools?.count ?? 0
        XCTAssert(count == schoolCreateCount, "Expected \(schoolCreateCount) schools but found \(count)")
    }
    
    func testUpdateSchools() {

        // given
        let presenter = SchoolsPresenter(storeInMemory: true)
        addSomeSchools(to: presenter)
        
        // when
        addSomeSchools(to: presenter)
        
        // then
        let count = presenter.schools?.count ?? 0
        XCTAssert(count == schoolCreateCount, "Expected \(schoolCreateCount) schools but found \(count)")
    }
    
    func testSchoolAtIndexWhenNoSchools() {
        
        // given
        let presenter = SchoolsPresenter(storeInMemory: true)
        
        // then
        XCTAssert(presenter.schoolAtIndex(0) == nil, "Found a school, array should be empty")
    }
    
    func testschoolAtIndexRequestedIsOutOfBounds() {
        
        // given
        let presenter = SchoolsPresenter(storeInMemory: true)
        
        // when
        addSomeSchools(to: presenter)
        
        // then
        let index = schoolCreateCount + 1
        XCTAssert(presenter.schoolAtIndex(index) == nil, "Found a school, shouldn't be a school at index \(index)")
    }
    
    func testSchoolAtIndexRequestedIsNegative() {
        
        // given
        let presenter = SchoolsPresenter(storeInMemory: true)
        
        // when
        addSomeSchools(to: presenter)
        
        // then
        let index = -1
        XCTAssert(presenter.schoolAtIndex(index) == nil, "Found a school, shouldn't be a school at index \(index)")
    }
    
    func testSchoolAtIndex() {
        
        // given
        let presenter = SchoolsPresenter(storeInMemory: true)
        
        // when
        addSomeSchools(to: presenter)
        
        // then
        let index = schoolCreateCount - 1
        XCTAssert(presenter.schoolAtIndex(index) != nil, "Didn't find a school at index \(index)")
    }
    
    func testSchoolsAreFetched() {
        
        // given
        let presenter = SchoolsPresenter(storeInMemory: true)
        
        let bundle = Bundle(for: type(of: self))
        let mockResponseUrl = bundle.url(forResource: "schools_list", withExtension: "json")!
        let returnedData = try? Data(contentsOf: mockResponseUrl)
        
        let config = URLSessionConfiguration.ephemeral
        config.protocolClasses = [URLProtocolMock.self]
        let session = URLSession(configuration: config)
        
        let expectation = XCTestExpectation(description: "School pull occurs")
        URLProtocolMock.returnedData = returnedData!
        
        // when
        presenter.updateSchools(session: session) { result in
            
            switch result {
            case .success():
                expectation.fulfill()
                
            case .failure(let error):
                print("failure \(error.localizedDescription)")
            }
        }
        
        // then
        wait(for: [expectation], timeout: 2.0)
    }
    
    func testGradeForSchoolIsFetched() {

        // given
        let presenter = SchoolsPresenter(storeInMemory: true)
        
        let bundle = Bundle(for: type(of: self))
        let mockResponseUrl = bundle.url(forResource: "school_grades", withExtension: "json")!
        let returnedData = try? Data(contentsOf: mockResponseUrl)
        
        let config = URLSessionConfiguration.ephemeral
        config.protocolClasses = [URLProtocolMock.self]
        let session = URLSession(configuration: config)
        
        let expectation = XCTestExpectation(description: "School grades pull occurs")
        URLProtocolMock.returnedData = returnedData!
        
        let grades = SchoolGrades("1", reading: "2", maths: "3", writing: "4")
        let school = School(name: "school", dbn: "1", grades: grades, city: "1", latitude: "2", longitude: "3", neighborhood: "4", website: "website", zip: "postcode")
        
        // when
        presenter.updateSchoolGrades(school: school, session: session) { result in
            
            switch result {
            case .success(let _):
                expectation.fulfill()
                
            case .failure(let error):
                print("failure \(error.localizedDescription)")
            }
        }
        
        // then
        wait(for: [expectation], timeout: 2.0)
    }
    
    func testGradeForSchoolIsFetchedAndUpdated() {
        
        // given
        let presenter = SchoolsPresenter(storeInMemory: true)
        
        let bundle = Bundle(for: type(of: self))
        let mockResponseUrl = bundle.url(forResource: "school_grades", withExtension: "json")!
        let returnedData = try? Data(contentsOf: mockResponseUrl)
        
        let config = URLSessionConfiguration.ephemeral
        config.protocolClasses = [URLProtocolMock.self]
        let session = URLSession(configuration: config)
        
        let expectation = XCTestExpectation(description: "School grades pull occurs and grades updated")
        URLProtocolMock.returnedData = returnedData!
        
        let grades = SchoolGrades("1", reading: "2", maths: "3", writing: "4")
        let school = School(name: "school", dbn: "1", grades: grades, city: "1", latitude: "2", longitude: "3", neighborhood: "4", website: "website", zip: "postcode")
        
        // when
        presenter.updateSchoolGrades(school: school, session: session) { result in
            
            switch result {
            case .success(let school):
                if let numTakers = school.grades?.num_of_sat_test_takers,
                    let maths = school.grades?.sat_math_avg_score,
                    let reading = school.grades?.sat_critical_reading_avg_score,
                    let writing = school.grades?.sat_writing_avg_score {
                    
                    // Values in school_grades.json
                    if numTakers == "29" &&
                        maths == "404" &&
                        reading == "355" &&
                        writing == "363" {
                        
                        expectation.fulfill()
                    }
                }
                
            case .failure(let error):
                print("failure \(error.localizedDescription)")
            }
        }
        
        // then
        wait(for: [expectation], timeout: 2.0)
    }
    
    func testFilterSchoolsEmptyString() {
        
        // given
        let presenter = SchoolsPresenter(storeInMemory: true)
        
        // when
        addSomeSchools(to: presenter)
        
        // then
        presenter.filterSchools(on: "")
        XCTAssert(presenter.filteredSchools.count == presenter.schools?.count ?? 0, "Filtered empty string but count of filtered doesnt match original list")
    }
    
    func testFilterSchoolsUnique() {
        
        // given
        let presenter = SchoolsPresenter(storeInMemory: true)
        
        // when
        addSomeSchools(to: presenter)
        
        // then
        presenter.filterSchools(on: "\(schoolCreateCount - 1)")
        XCTAssert(presenter.filteredSchools.count == 1, "Filtered empty string on 1 unique char, but count of filtered doesnt equal 1")
    }
    
    func addSomeSchools(to presenter: SchoolsPresenter) {
        
        var schoolsArray: [School] = []
        
        for ix in 0..<schoolCreateCount {
            let grades = SchoolGrades("1", reading: "2", maths: "3", writing: "4")
            let school = School(name: "School\(ix)", dbn: "\(ix)", grades: grades, city: "Ripon", latitude: "1.01", longitude: "1.01", neighborhood: "Yorkshire", website: "karmatoad.com", zip: "HG4")
            schoolsArray.append(school)
        }
        
        presenter.storage.updateSchools(schoolsArray)
    }

}
