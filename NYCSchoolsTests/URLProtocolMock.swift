//
//  URLProtocolMock.swift
//  NYCSchoolsTests
//
//  Created by John Ward on 07/02/2019.
//  Copyright © 2019 John Ward. All rights reserved.
//

import Foundation

class URLProtocolMock: URLProtocol {
    
    static var returnedData: Data?
    
    override class func canInit(with request: URLRequest) -> Bool {
        return true
    }
    
    override class func canonicalRequest(for request: URLRequest) -> URLRequest {
        return request
    }
    
    override func startLoading() {
        self.client?.urlProtocol(self, didLoad: URLProtocolMock.returnedData ?? Data())
        self.client?.urlProtocolDidFinishLoading(self)
    }
    
    override func stopLoading() {
        
    }
}
