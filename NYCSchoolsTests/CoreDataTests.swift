//
//  CoreDataTests.swift
//  NYCSchoolsTests
//
//  Created by John Ward on 07/02/2019.
//  Copyright © 2019 John Ward. All rights reserved.
//

import XCTest
@testable import NYCSchools

class CoreDataTests: XCTestCase {
    
    let schoolCreateCount = 3
    
    override func setUp() {
    }
    
    override func tearDown() {
    }
    
    func testCreateSchoolsCount() {
        
        // given
        let storage = CoreData(storeInMemory: true)
        
        // when
        addSomeSchools(to: storage)
        
        // then
        let count = storage.schools?.count ?? 0
        
        XCTAssert(count == schoolCreateCount, "Added \(schoolCreateCount) schools but only found \(count)")
    }
    
    func testCreateSchoolsCountAfterUpdate() {
        
        // given
        let storage = CoreData(storeInMemory: true)
        addSomeSchools(to: storage)
        
        // when
        addSomeSchools(to: storage)
        
        // then
        let count = storage.schools?.count ?? 0
        
        XCTAssert(count == schoolCreateCount, "Added \(schoolCreateCount) schools but only found \(count)")
    }
    
    func testCreateSchoolsAndSchoolsMatchValuesAndSequence() {
        
        // given
        let storage = CoreData(storeInMemory: true)
        let schools = schoolArray()
        
        // when
        storage.updateSchools(schools)
        
        // then
        for (index, school) in schools.enumerated() {
            if let storedSchools = storage.schools {
                let storedSchool = storedSchools[index]
                XCTAssert(school == storedSchool, "School at index \(index) doesnt match - \(school) \(storedSchool)")
            } else {
                XCTFail("Didnt find school stored at index \(index)")
            }
        }
        
    }
    
    func addSomeSchools(to storage: CoreData) {
        
        storage.updateSchools(schoolArray())
    }
    
    func schoolArray() -> [School] {
        
        var schoolArray: [School] = []
        
        for ix in 0..<schoolCreateCount {
            let grades = SchoolGrades("1", reading: "2", maths: "3", writing: "4")
            let school = School(name: "School\(ix)", dbn: "\(ix)", grades: grades, city: "Ripon", latitude: "1.01", longitude: "1.01", neighborhood: "Yorkshire", website: "karmatoad.com", zip: "HG4")
            schoolArray.append(school)
        }
        
        return schoolArray
    }
    
}
